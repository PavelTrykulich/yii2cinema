<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Film;

/**
 * FilmSearch represents the model behind the search form of `common\models\Film`.
 */
class FilmSearch extends Film
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'year', 'age', 'producer_id'], 'integer'],
            [['title', 'country', 'picture', 'start_show', 'budget', 'description', 'trailer_link', 'company'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Film::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'start_show' => $this->start_show,
            'year' => $this->year,
            'age' => $this->age,
            'producer_id' => $this->producer_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'budget', $this->budget])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'trailer_link', $this->trailer_link])
            ->andFilterWhere(['like', 'company', $this->company]);

        return $dataProvider;
    }
}
