<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FilmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="film-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'country') ?>

    <?= $form->field($model, 'picture') ?>

    <?= $form->field($model, 'start_show') ?>

    <?php // echo $form->field($model, 'budget') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'trailer_link') ?>

    <?php // echo $form->field($model, 'company') ?>

    <?php // echo $form->field($model, 'year') ?>

    <?php // echo $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'producer_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
