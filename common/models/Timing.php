<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "timing".
 *
 * @property int $id
 * @property string $date_show
 * @property string $length
 * @property int $price
 * @property int $film_id
 * @property int $hall_id
 *
 * @property Film $film
 * @property Hall $hall
 */
class Timing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'film_id', 'hall_id'], 'required'],
            [['id', 'price', 'film_id', 'hall_id'], 'integer'],
            [['date_show', 'length'], 'safe'],
            [['id'], 'unique'],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Film::className(), 'targetAttribute' => ['film_id' => 'id']],
            [['hall_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hall::className(), 'targetAttribute' => ['hall_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_show' => 'Date Show',
            'length' => 'Length',
            'price' => 'Price',
            'film_id' => 'Film ID',
            'hall_id' => 'Hall ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Film::className(), ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHall()
    {
        return $this->hasOne(Hall::className(), ['id' => 'hall_id']);
    }
}
