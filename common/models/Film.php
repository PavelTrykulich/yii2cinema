<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "film".
 *
 * @property int $id
 * @property string $title
 * @property string $country
 * @property string $picture
 * @property string $start_show
 * @property string $budget
 * @property string $description
 * @property string $trailer_link
 * @property string $company
 * @property int $year
 * @property int $age
 * @property int $producer_id
 *
 * @property Producer $producer
 * @property FilmGenre[] $filmGenres
 * @property Timing[] $timings
 */
class Film extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'film';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'producer_id'], 'required'],
            [['id', 'year', 'age', 'producer_id'], 'integer'],
            [['start_show'], 'safe'],
            [['description'], 'string'],
            [['title', 'picture', 'budget', 'trailer_link', 'company'], 'string', 'max' => 255],
            [['country'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['producer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producer::className(), 'targetAttribute' => ['producer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'country' => 'Country',
            'picture' => 'Picture',
            'start_show' => 'Start Show',
            'budget' => 'Budget',
            'description' => 'Description',
            'trailer_link' => 'Trailer Link',
            'company' => 'Company',
            'year' => 'Year',
            'age' => 'Age',
            'producer_id' => 'Producer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducer()
    {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilmGenres()
    {
        return $this->hasMany(FilmGenre::className(), ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimings()
    {
        return $this->hasMany(Timing::className(), ['film_id' => 'id']);
    }

    public function showGenres()
    {
        $arr = [];
        foreach ($this->filmGenres as $filmGenre) {
            $arr[] = $filmGenre->genre->title;
        }

        return implode(", ", $arr);
    }

    public function showAge()
    {
        return $this->age . '+';
    }

    public function showBudget()
    {
        return $this->budget. ' $';
    }

    public function showProducer()
    {
        return $this->producer->name . ' (' . $this->producer->country . ')';
    }

    public function showStartShow(){

        if($this->start_show > date('Y-m-d')){
        return 'В кино с ' . $this->start_show;
        }else{
            return 'Сейчас в показе';
        }
    }

    public function top(){


    }

}
