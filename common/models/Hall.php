<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hall".
 *
 * @property int $id
 * @property string $title
 * @property int $number_places
 * @property int $cinema_id
 *
 * @property Cinema $cinema
 * @property Timing[] $timings
 */
class Hall extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hall';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cinema_id'], 'required'],
            [['id', 'number_places', 'cinema_id'], 'integer'],
            [['title'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['cinema_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cinema::className(), 'targetAttribute' => ['cinema_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'number_places' => 'Number Places',
            'cinema_id' => 'Cinema ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCinema()
    {
        return $this->hasOne(Cinema::className(), ['id' => 'cinema_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimings()
    {
        return $this->hasMany(Timing::className(), ['hall_id' => 'id']);
    }
}
