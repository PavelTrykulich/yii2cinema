<?php


/* @var $films common\models\Film[] */
/* @var $pagination frontend\controllers\FilmController*/
use yii\widgets\LinkPager;
?>



<div class="body-content">

    <div class="row" align="center">

        <?php foreach ($films as $film): ?>

            <div class="col-lg-4">
                <h2><?=$film->title ?></h2>
                <img src="<?='/resource/img/films/' . $film->picture ?>" width="189" height="255">
                <p><a class="btn btn-default" href="info-film?id=<?= $film->id ?>">Подробно</a></p>
            </div>

        <?php endforeach; ?>

    </div>

    <div class="pagination" align="center">
        <?= LinkPager::widget(['pagination' => $pagination]);  ?>
    </div>

</div>