<?php
/* @var $film common\models\Film */
?>

<iframe width="1140" height="630"
        src="<?= $film->trailer_link ?>" frameborder="0" allowfullscreen>
</iframe>

<?php if(!empty($film->timings)): ?>
<div class="panel panel-default">

    <div class="panel-heading"><h1>Расписание</h1></div>

    <table class="table table-bordered table-dark">
        <thead>
        <tr>
            <th scope="col">Кинотеатр</th>
            <th scope="col">Дата и время</th>
            <th scope="col">Зал</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($film->getTimings()->orderBy(['date_show' => SORT_ASC])->all() as $timing): ?>
            <tr>
                <td><?= $timing->hall->cinema->title ?></td>
                <td><?= $timing->date_show ?></td>
                <td><?= $timing->hall->title ?></td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<?php else:  ?>
    <div align="center">
        <h1> Показ завершен</h1>
    </div>
<?php endif; ?>

<div align="center">
    <img src=" <?='/resource/img/films/' . $film->picture ?> " width="567" height="765" >
    <h2><?=$film->title ?></h2>
    <h2 ><?=$film->showAge() ?></h2>
    <p><button type="button" class="btn btn-success"><h3>Купить билет</h3></button></p>

    <div class="jumbotron">
        <h3>Описание</h3>
        <p><?= $film->description ?></p>

    </div>

    <table class="table table-bordered">
        <tbody>
        <tr>
            <td>Жанр</td>
            <td>
                <?= $film->showGenres(); ?>
            </td>
        </tr>
        <tr>
            <td>Год</td>
            <td><?=$film->year ?></td>
        </tr>
        <tr>
            <td>Старт показа</td>
            <td><?=$film->start_show ?></td>
        </tr>
         <tr>
            <td>Страна</td>
            <td><?=$film->country ?></td>
        </tr>
         <tr>
            <td>Компания</td>
            <td><?=$film->company ?></td>
        </tr>
        <tr>
            <td>Продюсер</td>
            <td><?=$film->showProducer() ?></td>
        </tr>
        <tr>
            <td>Бюджет</td>
            <td><?=$film->showBudget() ?></td>
        </tr>

        </tbody>
    </table>

</div>

<pre>
    <?= var_dump($film->timings) ?>
    <?= print_r($film->filmGenres)?>
</pre>
