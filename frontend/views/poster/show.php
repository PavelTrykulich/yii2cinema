<?php

/* @var $films_new common\models\Film[] */
/* @var $films_now common\models\Film[] */

use yii\widgets\LinkPager;
use yii\helpers\Url;

?>



<div class="body-content">

    <div class="row" align="center">

        <h1>Ожидаемые фильмы</h1>

        <?php foreach ($films_new as $film): ?>

            <div class="col-lg-4">
                <h2><?= $film->title ?></h2>
                <h4><?= $film->showAge() ?></h4>
                <h4><?= $film->showStartShow() ?></h4>
                <img src="<?='/resource/img/films/' . $film->picture ?>" width="189" height="255">
               <!-- <p><a class="btn btn-default" href="info-film?id=<?/*= $film->id */?>">Подробно</a></p>
                <p><a class="btn btn-default" href="<?/*= Yii::$app->urlManager->createUrl(['film/info-film', 'id' => $film->id]) */?>">Подробно</a></p>
               -->
                <p><a class="btn btn-default" href="<?= Url::to(['film/info-film', 'id' => 5]) ?>">Подробно</a></p>
            </div>

        <?php endforeach; ?>

    </div>
</div>

    <div class="body-content">

        <div class="row" align="center">

            <h1>Смотрите сейчас</h1>

            <?php foreach ($films_now as $film): ?>

                <div class="col-lg-4">
                    <h2><?= $film->title ?></h2>
                    <h4><?= $film->showAge() ?></h4>
                    <h4><?= $film->showStartShow() ?></h4>
                    <img src="<?='/resource/img/films/' . $film->picture ?>" width="189" height="255">
                    <p><a class="btn btn-default" href="info-film?id=<?= $film->id ?>">Подробно</a></p>
                </div>

            <?php endforeach; ?>

        </div>

    </div>
