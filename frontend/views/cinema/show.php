<?php
/* @var $cinemas common\models\Cinema[] */
?>

<div class="body-content">

    <div class="row" align="center">
        <?php foreach ($cinemas as $cinema): ?>

            <div class="col-lg-12">
                <h2><strong><?= $cinema->title ?></strong></h2>
                <img src="<?= '/resource/img/cinemas/' . $cinema->picture ?>" width="800" height="620">
                <h5>Адрес <?= $cinema->address ?></h5>
                <br>
            </div>

        <?php endforeach;?>
    </div>

    <div class="pagination" align="center">

    </div>

</div>
