<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.07.18
 * Time: 11:03
 */

namespace frontend\controllers;


use yii\db\Query;
use yii\web\Controller;
use common\models\Film;
use yii\data\Pagination;

class PosterController extends Controller
{
    /**
     * @return string
     * @throws \yii\db\Exception
     */


    public function actionShow()
    {
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');

        $films_new = Film::find()
            ->joinWith('timings')
            ->where(['is not', 'film_id', null])
            ->andWhere(['>', 'start_show', $today])
            ->orderBy(['start_show' => SORT_DESC])
            ->groupBy('`film`.`id`');

        $pagination_new = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $films_new->count()
        ]);

        $films_new = $films_new->offset($pagination_new->offset)
            ->limit($pagination_new->limit)
            ->all();


        $films_now = Film::find()
            ->joinWith('timings')
            ->where(['is not', 'film_id', null])
            ->andWhere(['<=', 'start_show', $today])
            ->orderBy(['start_show' => SORT_DESC])
            ->groupBy('`film`.`id`');

        $pagination_now = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $films_now->count()
        ]);

        $films_now = $films_now->offset($pagination_now->offset)
            ->limit($pagination_now->limit)
            ->all();




        return $this->render('show', [
            'films_new' => $films_new,
            'films_now' => $films_now,
            'pagination_new' => $pagination_new,
            'pagination_old' => $pagination_now
        ]);
    }

    public function actionInfoFilm($id)
    {
        $film = Film::find()->where(['id'=> $id])->limit(1)->one();

        return $this->render('infoFilm', compact('film'));
    }




}