<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.07.18
 * Time: 10:44
 */

namespace frontend\controllers;


use common\models\Cinema;
use yii\web\Controller;

class CinemaController extends Controller
{
    public function actionShow()
    {
        $cinemas = Cinema::find()->all();

        return $this->render('show', compact('cinemas'));
    }
}