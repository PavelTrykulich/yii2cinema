<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.07.18
 * Time: 10:44
 */

/* @var $films common\models\Film[] */

namespace frontend\controllers;


use common\models\Film;
use yii\web\Controller;
use yii\data\Pagination;

class FilmController extends Controller
{
    public function actionShow()
    {
        $films = Film::find()->all();

        return $this->render('show', compact('films'));
    }

    /**
     * @return string
     */
    public function actionAllFilms()
    {
        $films = Film::find();


        $pagination = new Pagination([
            'defaultPageSize' => 12,
            'totalCount' => $films->count()
        ]);
        $films = $films->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('allFilms', [
            'films' => $films,
            'pagination' => $pagination
        ]);
    }

    public function actionInfoFilm($id)
    {
        $film = Film::find()->where(['id'=> $id])->limit(1)->one();

        return $this->render('infoFilm', compact('film'));
    }



}