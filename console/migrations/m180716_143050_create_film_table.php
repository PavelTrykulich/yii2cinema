<?php

use yii\db\Migration;

/**
 * Handles the creation of table `film`.
 */
class m180716_143050_create_film_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql ="
        SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
        SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
        SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
        
        CREATE TABLE IF NOT EXISTS `producer` (
         `id` INT NOT NULL,
         `name` VARCHAR(100) NULL,
         `country` VARCHAR(45) NULL,
          PRIMARY KEY (`id`))
        ENGINE = InnoDB;
        
        CREATE TABLE IF NOT EXISTS `film` (
          `id` INT NOT NULL,
          `title` VARCHAR(255) NULL,
          `country` VARCHAR(45) NULL,
          `picture` VARCHAR(255) NULL,
          `start_show` DATE NULL,
          `budget` VARCHAR(255) NULL,
          `description` TEXT NULL,
          `trailer_link` VARCHAR(255) NULL,
          `company` VARCHAR(255) NULL,
          `year` INT NULL,
          `age` INT NULL,
          `producer_id` INT NOT NULL,
           PRIMARY KEY (`id`),
          INDEX `fk_film_producer1_idx` (`producer_id` ASC),
          CONSTRAINT `fk_film_producer1`
            FOREIGN KEY (`producer_id`)
            REFERENCES `producer` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
          ENGINE = InnoDB;
          
          CREATE TABLE IF NOT EXISTS `cinema` (
              `id` INT NOT NULL,
              `title` VARCHAR(45) NULL,
              `address` VARCHAR(255) NULL,
              `picture` VARCHAR(255) NULL,
               PRIMARY KEY (`id`))
          ENGINE = InnoDB;
          
          CREATE TABLE IF NOT EXISTS `hall` (
            `id` INT NOT NULL,
            `title` VARCHAR(45) NULL,
            `number_places` INT NULL,
            `cinema_id` INT NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_hall_cinema1_idx` (`cinema_id` ASC),
          CONSTRAINT `fk_hall_cinema1`
           FOREIGN KEY (`cinema_id`)
           REFERENCES `cinema` (`id`)
           ON DELETE NO ACTION
           ON UPDATE NO ACTION)
          ENGINE = InnoDB;
          
          CREATE TABLE IF NOT EXISTS `timing` (
              `id` INT NOT NULL,
              `date_show` DATETIME NULL,
              `length` TIME NULL,
              `price` INT NULL,
              `film_id` INT NOT NULL,
              `hall_id` INT NOT NULL,
          PRIMARY KEY (`id`),
          INDEX `fk_timing_film1_idx` (`film_id` ASC),
          INDEX `fk_timing_hall1_idx` (`hall_id` ASC),
          CONSTRAINT `fk_timing_film1`
          FOREIGN KEY (`film_id`)
          REFERENCES `film` (`id`)
          ON DELETE NO ACTION
          ON UPDATE NO ACTION,
         CONSTRAINT `fk_timing_hall1`
            FOREIGN KEY (`hall_id`)
            REFERENCES `hall` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)    
         ENGINE = InnoDB;
         
         CREATE TABLE IF NOT EXISTS `genre` (
        `id` INT NOT NULL,
        `title` VARCHAR(45) NULL,
        PRIMARY KEY (`id`))
        ENGINE = InnoDB;
        
        CREATE TABLE IF NOT EXISTS `catagory` (
            `id` INT NOT NULL,
            `film_id` INT NOT NULL,
        PRIMARY KEY (`id`),
         INDEX `fk_catagory_film1_idx` (`film_id` ASC),
         CONSTRAINT `fk_catagory_film1`
         FOREIGN KEY (`film_id`)
         REFERENCES `film` (`id`)
         ON DELETE NO ACTION
         ON UPDATE NO ACTION)
         ENGINE = InnoDB;
         
         CREATE TABLE IF NOT EXISTS `film_genre` (
          `film_id` INT NOT NULL,
          `genre_id` INT NOT NULL,
         INDEX `fk_film_genre_film1_idx` (`film_id` ASC),
         INDEX `fk_film_genre_genre1_idx` (`genre_id` ASC),
         CONSTRAINT `fk_film_genre_film1`       
         FOREIGN KEY (`film_id`)
         REFERENCES `film` (`id`)
         ON DELETE NO ACTION
         ON UPDATE NO ACTION,
         CONSTRAINT `fk_film_genre_genre1`
         FOREIGN KEY (`genre_id`)
         REFERENCES `genre` (`id`)
         ON DELETE NO ACTION
         ON UPDATE NO ACTION)
         ENGINE = InnoDB;


         SET SQL_MODE=@OLD_SQL_MODE;
         SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
         SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
        ";


        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('film');
    }
}
